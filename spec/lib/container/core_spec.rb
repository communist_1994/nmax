require 'spec_helper'
require 'pry'

RSpec.describe Container::Core do
  describe '#insert' do

    context 'when container is empty' do
      let(:core){ Container::Core.new(8) }
      before do
        core.insert(5)
      end
      it 'insert numer in tail' do
        expect(core.tail.value).to eq(5)
      end
      it 'change real_size to 1' do
        expect(core.real_size).to eq(1)
      end
      it 'add numer in container' do
        expect(core.to_a).to eq([5])
      end
    end

    context 'when insert in center' do
      let(:core){ Container::Core.new(5) }
      before do
        [1,3,5].each{|a| core.insert(a)}
        core.insert(4)
      end
      it 'insert numer in tail' do
        expect(core.tail.value).to eq(1)
      end
      it 'change real_size to 4' do
        expect(core.real_size).to eq(4)
      end
      it 'add numer in container' do
        expect(core.to_a).to eq([1, 3, 4, 5])
      end
    end

    context 'when insert in tail' do
      let(:core){ Container::Core.new(5) }
      before do
        [1,3,5].each{|a| core.insert(a)}
        core.insert(0)
      end
      it 'insert numer in tail' do
        expect(core.tail.value).to eq(0)
      end
      it 'change real_size to 4' do
        expect(core.real_size).to eq(4)
      end
      it 'add numer in container' do
        expect(core.to_a).to eq([0, 1, 3, 5])
      end
    end
    context 'when insert in head' do
      let(:core){ Container::Core.new(3) }
      before do
        [1,3,5].each{|a| core.insert(a)}
        core.insert(6)
      end
      it 'insert numer in tail' do
        expect(core.tail.value).to eq(3)
      end
      it 'change real_size to 4' do
        expect(core.real_size).to eq(3)
      end
      it 'add numer in container' do
        expect(core.to_a).to eq([3, 5, 6])
      end
    end
  end
end
