module Container
  class Cell
    attr_reader :value
    attr_accessor :next
    
    def initialize(numer, object = nil)
      @value = numer
      @next = object
    end
  end
end
