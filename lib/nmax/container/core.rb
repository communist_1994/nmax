module Container
  class Core
    attr_reader :real_size, :tail, :target_size
    def initialize(size)
      @target_size = size
      @real_size = 0
      @tail = nil
    end

    def insert(numer)
      if tail.nil?
        @tail = Cell.new(numer)
        @real_size = 1
        return
      end
      empty_insert(numer)
      cut_tail if real_size > target_size
    end

    def to_a
      result = []
      last = @tail
      while last
        result.push(last.value)
        last = last.next
      end
      result
    end

    private

    def cut_tail
      @real_size -= 1
      @tail = @tail.next
    end

    def empty_insert(numer)
      return if numer == @tail.value
      if @tail.value > numer
        @tail = Cell.new(numer, @tail)
        @real_size += 1
      else
        before_cell = @tail
        while true
          next_cell = before_cell.next
          return if next_cell && numer == next_cell.value
          if next_cell.nil? || numer < next_cell.value
            before_cell.next = Cell.new(numer, next_cell)
            @real_size += 1
            return
          end
          before_cell = next_cell
        end
      end
    end
  end
end
