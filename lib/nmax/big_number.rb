class BigNumber
  attr_reader :source

  def initialize(source)
    @source = source
  end

  def >(another_number)
  end

  def <(another_number)
  end

  def ==(another_number)
  end

  def size
    source.size
  end
end
